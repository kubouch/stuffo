"""Top-level Stuffo API controlled by CLI.
"""

from pathlib import Path

from .fileutils import suffix_files
from .inventory import Inventory

# Later move to config file inside inventory dir
# *_TO_IGNORE are full names now, they should be regexes later
DIRS_TO_IGNORE = []
FILES_TO_IGNORE = []
ITEM_FILE_SUFFIXES = [".txt"]


def find_inventory(path):
	"""Check if a 'path' is inside an inventory
	"""

	# Search upwards for a special file
	return path


def get_inventory(path_str):
	p = Path(path_str).resolve()

	if not p.exists():
		raise FileNotFoundError("Path {} does not exist.".format(p))
	elif not p.is_dir():
		raise NotADirectoryError("{} is not a directory.".format(p))

	inv_path = find_inventory(p)  # root inventory path can be somwhere upwards
	if inv_path:
		return Inventory(inv_path, DIRS_TO_IGNORE, FILES_TO_IGNORE, ITEM_FILE_SUFFIXES)
	else:
		# prompt for inventory creation
		raise ValueError("Not in inventory")
