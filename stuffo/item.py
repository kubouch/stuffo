"""
Item class
"""


class Item:

	# Default attributes can be used as search or sort keys (Item.name, ...)
	file_path = None
	name = None
	categories = None
	count = None

	def __init__(self, file_path, name, categories, count=1, subitems=[]):
		self.file_path = file_path
		self.name = name
		self.categories = categories
		self.count = count
		self.subitems = subitems
