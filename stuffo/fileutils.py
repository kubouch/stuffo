"""
File searching and reading.
"""

import os

from pathlib import Path


def subdirs(start_path, depth=-1, ignorelist=[], include_top=True):
	"""Yields all subdirectories of 'start_path' to a depth specified by
	'depth'. Ignores directory names in ignorelist.

	Output Path objects might, but are not guaranteed to be, sorted.

	Args:
		start_path (pathlib.Path): Starting directory path.
		depth (int): How many levels to descend. -1 descends all.
		ignorelist (list of str): Directories with names in ignorelist are not
			yielded.
		include_top (bool): Include the 'start_path'

	Yields:
		pathlib.Path: Path object of the subdirectory.
	"""

	if not isinstance(start_path, Path):
		start_path = Path(start_path)

	if not start_path.is_dir():
		raise NotADirectoryError("{} is not a directory.".format(str(start_path)))

	if include_top:
		yield start_path

	for child in start_path.iterdir():
		if all([child.is_dir(), child.name not in ignorelist]):
			if depth == 0:
				break
			else:
				yield child
				yield from subdirs(child, depth-1, ignorelist, False)


def suffix_files(path, depth, file_suffixes,
                 dirs_to_ignore=[], files_to_ignore=[]):
	if not isinstance(path, Path):
		path = Path(path)
	if not path.is_dir():
		raise NotADirectoryError("{} is not a directory.".format(str(path)))
	for subdir in subdirs(path, depth=depth, ignorelist=dirs_to_ignore):
		for f in os.listdir(subdir):
			file_path = Path(subdir).joinpath(f)
			if file_path.is_file() and (f not in files_to_ignore):
				if any([f.endswith(s) for s in file_suffixes]):
					yield file_path


def lines_in_files(path, depth, file_suffixes,
                   dirs_to_ignore=[], files_to_ignore=[]):
	if not isinstance(path, Path):
		path = Path(path)
	if not path.is_dir():
		raise NotADirectoryError("{} is not a directory.".format(str(path)))
	for f in suffix_files(path, depth, file_suffixes, dirs_to_ignore, files_to_ignore):
		with open(f, 'r') as rf:
			for line in rf:
				if line: yield line
