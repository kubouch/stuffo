"""
Inventory class
"""

from pathlib import Path

import regex

from .fileutils import suffix_files
from .item import Item


class InvalidLineError(Exception):
	pass


def _parse_line(line):
	"""Parse a line according to a file format and store data into 'item_props'
	dictionary.
	"""

	line = line.strip()

	item_props = {'name': None, 'count': None, 'owners': None}

	if line:
		if line.startswith('#'):
			pass  # Comment
		else:
			# Match on integers surrounded by whitespace (or EOL)
			pattern = "(\s[0-9]+(\s|$))"
			# Split around the match
			parts = regex.split(pattern, line)
			# Filter out empty strings
			parts = [s.strip() for s in parts if s.strip()]
			if len(parts) < 2 or len(parts) > 3:
				raise InvalidLineError("The following line is invalid:\n{}"\
				                       .format(line))
			item_props['name'] = parts[0]
			item_props['count'] = int(parts[1])
	else:
		pass  # Empty line

	return item_props


class Inventory:
	def __init__(self, path,
		         dirs_to_ignore=[], files_to_ignore=[], item_file_suffixes=[]):
		self.root_path = Path(path).resolve()
		self.dirs_to_ignore = dirs_to_ignore
		self.files_to_ignore = files_to_ignore
		self.item_file_suffixes = item_file_suffixes
		self.items = []

	def find(self, name, where=Item.name):
		pass

	def populate(self, depth):
		if not self.items:
			for f in suffix_files(self.root_path, depth,
			                      self.item_file_suffixes,
			                      self.dirs_to_ignore, self.files_to_ignore):
				categories = f.relative_to(self.root_path).parts
				with open(f, 'r') as rf:
					for i, line in enumerate(rf):
						try:
							item_props = _parse_line(line)
							if item_props['name'] and item_props['count']:
								self.items.append(Item(f,
								                       item_props['name'],
								                       categories,
								                       item_props['count']))
							else:
								pass  # Empty line or comment
						except InvalidLineError as e:
							raise InvalidLineError("In file {} on line {}."\
							                       .format(str(f), i+1)) from e
		else:
			raise ValueError("Inventory already populated.")
