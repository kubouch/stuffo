"""Command line interface of Stuffo.
"""

import sys

import click

from . import main
from .__version__ import __version__


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS, invoke_without_command=True,
             help="Organize your stuff like a pro(grammer).")
@click.option('--version', is_flag=True,
              help="Display current version.")
@click.option('-p', '--path', default='.',
              help="Starting path. Defaults to the current path (pwd).")
@click.pass_context
def cli(ctx, version, path):
	"""The CLI entry point.
	"""

	if version:
		click.echo(__version__)
		sys.exit(0)
	else:
		if ctx.invoked_subcommand is None:
			click.echo("Missing command!\n")
			click.echo(ctx.get_help())
		else:
			ctx.obj = main.get_inventory(path)


@cli.command(help="List categories and/or items.")
@click.option('--levels', default=-1, show_default=True,
              help="Number of levels to list (negative lists all).")
@click.option('--flat', default=-1, show_default=True,
              help="Number of levels after which items are listed" +
                   " uncategorized (negative never flattens).")
@click.option('--categories', is_flag=True,
              help="Display only categories and ignore items.")
@click.pass_context
def list(ctx, levels, flat, categories):
	"""Lists categories and/or items over a specific number of levels.

	Args:
		levels (int): How many levels to descend. -1 descends all levels.
		flat (int): After how many levels stop displaying categories.
			When -1 is passed, categories are always shown
		categories (bool): Shows only categories and hides the items.
	"""

	click.echo("Listing {} levels deep.".format(levels))
	ctx.obj.populate(levels)
	cnt = 0
	for item in ctx.obj.items:
		click.echo("{}, {}".format(item.name, item.count))
		cnt += item.count
	click.echo("Total {}".format(cnt))


@cli.command(help="Put items to/from trash.")
@click.option('-r', '--restore', is_flag=True,
              help="Restore items from trash.")
@click.option('--show', is_flag=True,
              help="Show the trash.")
@click.argument('items', nargs=-1)
@click.pass_context
def trash(ctx, items, restore, show):
	if show:
		ctx.invoke(list)
		sys.exit(0)
	if restore:
		click.echo("{} restored from trash.".format(items))
	else:
		click.echo("{} put to trash.".format(items))
