import pytest

from stuffo.inventory import _parse_line, InvalidLineError

class TestInventory:
	"""Tests for inventory.py.
	"""

	def test_parse_line(self):
		"""Tests for _parse_line()
		"""

		# One-word name
		line = "Socks 2"
		expected_out = {'name': "Socks",
		                'count': 2,
		                'owners': None }
		assert _parse_line(line) == expected_out

		# Multi-word name
		line = "Brown penguin socks 14"
		expected_out = {'name': "Brown penguin socks",
		                'count': 14,
		                'owners': None}
		assert _parse_line(line) == expected_out

		# No count (invalid)
		line = "Brown penguin socks"
		with pytest.raises(InvalidLineError):
			_parse_line(line)

		# More numbers surrounded by whitespace (invalid)
		line = "Brown 2 penguin socks 14"
		with pytest.raises(InvalidLineError):
			_parse_line(line)

		# Number in the beginning is fine
		line = "2 Brown penguin socks 14"
		expected_out = {'name': "2 Brown penguin socks",
		                'count': 14,
		                'owners': None}
		assert _parse_line(line) == expected_out

		# One owner (not implemented)
		line = "Brown penguin socks 14 John"
		expected_out = {'name': "Brown penguin socks",
		                'count': 14,
		                'owners': None}
		assert _parse_line(line) == expected_out
		# More owners (not implemented)
		line = "Brown penguin socks 14 John Joe Suzy"
		expected_out = {'name': "Brown penguin socks",
		                'count': 14,
		                'owners': None}
		assert _parse_line(line) == expected_out
		# Empty line (not implemented)
		line = "\n"
		expected_out = {'name': None,
		                'count': None,
		                'owners': None}
		assert _parse_line(line) == expected_out
		# Comment line (not implemented)
		line = "# There is a comment!"
		expected_out = {'name': None,
		                'count': None,
		                'owners': None}
		assert _parse_line(line) == expected_out
		# Inline comment (not implemented)
		line = "Brown penguin socks 14 # Cool socks"
		expected_out = {'name': "Brown penguin socks",
		                'count': 14,
		                'owners': None}
		assert _parse_line(line) == expected_out
