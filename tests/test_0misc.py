"""
This file has a weird name to ensure it gets tested first.

Apart from sanity tests it contains utility functions used in tests
"""

import subprocess
from collections import Counter

import pytest

class TestMisc:
	"""Various sanity and syntax checks.
	"""

	def test_help(self):
		"""Check if it even runs stuffo.
		"""

		subprocess.run(["stuffo", "--help"])

def equal_unordered(a, b):
	"""Tests whether two collections are equal or not, without order.

	Objects in collections need to be hashable or orderable.
	"""

	try:
		return Counter(a) == Counter(b)
	except TypeError:
		return sorted(a) == sorted(b)
