import os
from pathlib import Path

import pytest

from stuffo.fileutils import subdirs, suffix_files, lines_in_files
from tests.test_0misc import equal_unordered


class TestFileutils:
	"""Test functions in stuffo/fileutils.py

	It expects 'pytest' to be ran from stuffo root directory.
	"""

	def test_subdirs(self, tmpdir):
		"""Test subdirectory iteration
		"""

		# Try to pass a file, not a directory. Should not output even the start
		# path.
		dirs = []
		with pytest.raises(NotADirectoryError):
			for d in subdirs(Path("tests/test_fileutils.py")):
				dirs.append(str(d))
		assert dirs == []

		# Create testing directories
		test_dirs = ["foo", "foo/bar", "foo/bar/baz1", "foo/bar/baz2"]
		for d in test_dirs:
			os.mkdir(Path(tmpdir).joinpath(d))

		# Go through all dirs (without tmpdir).
		expected_dirs = ["foo", "foo/bar", "foo/bar/baz1", "foo/bar/baz2"]
		dirs = []
		for d in subdirs(Path(tmpdir), include_top=False):
			dirs.append(str(d.relative_to(tmpdir)))
		assert equal_unordered(dirs, expected_dirs)
		# The same with string input, not Path object
		dirs = []
		for d in subdirs(str(tmpdir), include_top=False):
			dirs.append(str(d.relative_to(tmpdir)))
		assert equal_unordered(dirs, expected_dirs)

		# Go through all dirs (with tmpdir)
		expected_dirs = [".", "foo", "foo/bar", "foo/bar/baz1", "foo/bar/baz2"]
		dirs = []
		for d in subdirs(Path(tmpdir), include_top=True):
			dirs.append(str(d.relative_to(tmpdir)))
		assert equal_unordered(dirs, expected_dirs)

		# Depth 0 (without tmpdir)
		expected_dirs = []
		dirs = []
		for d in subdirs(Path(tmpdir), depth=0, include_top=False):
			dirs.append(str(d.relative_to(tmpdir)))
		assert equal_unordered(dirs, expected_dirs)

		# Test ignorelist (without tmpdir)
		expected_dirs = ["foo", "foo/bar", "foo/bar/baz1"]
		dirs = []
		for d in subdirs(Path(tmpdir), include_top=False, ignorelist=["baz2"]):
			dirs.append(str(d.relative_to(tmpdir)))
		assert equal_unordered(dirs, expected_dirs)

	def test_suffix_files(self, tmpdir):
		"""Test file iteration in subdirs.
		"""

		# Try to pass a file, not a directory.
		files = []
		with pytest.raises(NotADirectoryError):
			for f in suffix_files(Path("tests/test_fileutils.py"), -1, [""]):
				files.append(f.name)
		assert files == []

		# Create testing directories and files
		test_dirs = ["foo", "foo/bar", "foo/bar/baz1", "foo/bar/baz2"]
		test_files = ["foo.tx", "foobar.txt", "foobarbaz1.sh", "foobarbaz2.sh"]
		for d, f in zip(test_dirs, test_files):
			dpath = Path(tmpdir).joinpath(d)
			os.mkdir(dpath)
			open(dpath.joinpath(f), 'a').close()

		# Accept all suffixes 2 levels deep
		expected_files = ["foo.tx", "foobar.txt"]
		files = []
		for f in suffix_files(Path(tmpdir), 2, [""]):
			files.append(f.name)
		assert equal_unordered(files, expected_files)

		# Go though files and filter them
		expected_files = ["foobarbaz2.sh"]
		files = []
		for f in suffix_files(Path(tmpdir), -1, [".txt", ".sh"],
		                      dirs_to_ignore=["baz1"],
		                      files_to_ignore=["foobar.txt"]):
			files.append(f.name)
		assert equal_unordered(files, expected_files)

		# The same with string input, not Path object
		files = []
		for f in suffix_files(str(tmpdir), -1, [".txt", ".sh"],
		                      dirs_to_ignore=["baz1"],
		                      files_to_ignore=["foobar.txt"]):
			files.append(f.name)
		assert equal_unordered(files, expected_files)

	def test_lines_in_files(self, tmpdir):
		"""Read lines in special files.
		"""

		# Test a) str input (not Path), b) file, not a directory
		lines = []
		with pytest.raises(NotADirectoryError):
			for f in lines_in_files("tests/test_fileutils.py", -1, [""]):
				lines.append(f.name)
		assert lines == []

		# Create testing directories and files and write lines to files
		test_dirs = ["foo", "foo/bar", "foo/bar/baz1", "foo/bar/baz2"]
		test_files = ["foo.tx", "foobar.txt", "foobarbaz1.sh", "foobarbaz2.sh"]
		test_lines = ["one line\n", "", "more\n\nlines\n", "\tspecial line\n"]
		for d, f, l in zip(test_dirs, test_files, test_lines):
			dpath = Path(tmpdir).joinpath(d)
			os.mkdir(dpath)
			with open(dpath.joinpath(f), 'w') as wf:
				wf.write(l)

		# Go through all files and gather lines
		expected_lines = ["one line\n", "more\n", "\n", "lines\n", "\tspecial line\n"]
		lines = []
		for line in lines_in_files(Path(tmpdir), -1, [".tx", ".txt", ".sh"]):
			lines.append(line)
		assert equal_unordered(lines, expected_lines)
