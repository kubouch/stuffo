#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from setuptools import setup, find_packages

# Get info from the __version__.py file (stolen from Requests)
repo_root = os.path.abspath(os.path.dirname(__file__))
about_path = os.path.join(repo_root, 'stuffo', '__version__.py')
about = {}
with open(about_path, 'r', encoding='utf-8') as f:
    exec(f.read(), about)

setup(
    name=about['__name__'],
    version=about['__version__'],
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    entry_points={
		"console_scripts": ["stuffo=stuffo.cli:cli"]
	},
	license='MIT',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython'
	]
)
