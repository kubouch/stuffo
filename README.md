# Stuffo

[![pipeline status](https://gitlab.com/kubouch/stuffo/badges/master/pipeline.svg)](https://gitlab.com/kubouch/stuffo/pipelines)
[![coverage report](https://gitlab.com/kubouch/stuffo/badges/master/coverage.svg)](https://gitlab.com/kubouch/stuffo/-/jobs)


Sort your things using folders, files, GitLab and Python.

### Notes

* **File syntax**: `thing_name    count    [owner1, owner2, ...]`
* pathlib implies Python 3.4
* os.mkdir (in tests) implies Python 3.6 for accepting Path objects
